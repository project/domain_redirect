<?php

/**
 * @file
 * Administrative page callbacks for the domain_redirect module.
 */
function _domain_redirect_generate_op_links($id) {
  $links = l(t('Edit'), "admin/build/domain/redirect/edit/$id") . '&nbsp;&nbsp;' .
  l(t('Delete'), "admin/build/domain/redirect/delete/$id");
  return $links;
}
function domain_redirect_list_redirects() {
  $headers = array(
    array(
      'data' => t('Title'),
      'field' => 'title',
      'sort' => 'asc'
    ),
    array(t('Operations'),
      'data' => t('Operations'),
    ),
  );
  $sql = "SELECT rid, title FROM {domain_redirect}";
  $sql .= tablesort_sql($headers);
  $limit = 50;
  $result = pager_query($sql, $limit);
  $rows = array();
  while($row = db_fetch_array($result)) {
    $rows[] = array(
      l($row['title'], 'domain-redirect/' . $row['rid']),
      _domain_redirect_generate_op_links($row['rid']),
    );
  }
  $output = '<div><ul class="item-list action-links"><li class="add first last">' . l(t('Add domain redirect'), 'admin/build/domain/redirect/add') . '</li></ul></div>';
  $output .= theme('table', $headers, $rows);
  $output .= theme('pager', NULL, $limit, 0);
  return $output;
}

function domain_redirect_foo_bar(&$form_state, $redirect = array()) {
}



/**
 * Redirect add/edit form
 */
function domain_redirect_edit_form(&$form_state, $redirect = array()) {
  // Merge default values.
  if (isset($redirect['settings'])) {
    $redirect['settings'] = unserialize($redirect['settings']);
  }
  $redirect += array(
    'rid' => NULL,
    'title' => '',
    'settings' => array(),
  );
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Create an internal link path that can be used in site pages and menu links. It can have a different destination depending on the domain that the user is browsing.'),
  );
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $redirect['rid'],
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Administrative title'),
    '#required' => TRUE,
    '#maxlength' => 255,
    '#default_value' => $redirect['title'],
  );
  $form['default_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Default destination path'),
    '#description' => t("Enter the default link path or full URL. (e.g. node/123 or http://www.example.com)."),
    '#size' => 42,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => $redirect['settings']['default_path'],
  );
  $form['domain_paths'] = array(
    '#type' => 'fieldset',
    '#title' => t('Domain-specific redirects'),
    '#description' => t("If this link should have a different redirect destination for a specific domain, enter it here. Any domains without an entry here will use the default destination."),
  );
  $domains = domain_domains();

  foreach ($domains as $key => $domain) {
    $form['domain_paths']['domain_id' . $key] = array(
      '#type' => 'textfield',
      '#title' => t($domain['sitename']),
      '#size' => 42,
      '#maxlength' => 255,
      '#required' => FALSE,
      '#default_value' => $redirect['settings']['domain_paths'][$key],
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['cancel'] = array(
    '#value' => l(t('Cancel'), isset($_REQUEST['destination']) ? $_REQUEST['destination'] : 'admin/build/domain/redirect'),
  );
  return $form;
}

function domain_redirect_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $redirect = array(
    'rid' => NULL,
    'title' => $values['title'],
    'settings' => array(
      'default_path' => $values['default_path'],
      'domain_paths' => array(),
    ),
  );
  foreach ($values as $key => $value) {
    if (strpos($key, 'domain_id') !== FALSE && !empty($value)) {
      $domain_id = substr($key, 9);
      $redirect['settings']['domain_paths'][$domain_id] = $value;
    }
  }
  domain_redirect_save($redirect);
  drupal_set_message(t('The redirect has been saved.'));
  $form_state['redirect'] = 'admin/build/domain/redirect';
}

function domain_redirect_delete_form($form_state, $redirect) {
  $form['redirect'] = array(
    '#type' => 'value',
    '#value' => $redirect,
  );
  $message = t('Are you sure you want to delete the redirect, %title?', array('%title' => $redirect['title']));
  $destination = isset($_REQUEST['destination']) ? $_REQUEST['destination'] : 'admin/build/domain/redirect';

  return confirm_form($form, $message, $destination);
}

function domain_redirect_delete_form_submit($form, &$form_state) {
  domain_redirect_delete($form_state['values']['redirect']['rid']);
  drupal_set_message(t('The redirect has been deleted.'));
  $form_state['redirect'] = 'admin/build/domain/redirect';
}
