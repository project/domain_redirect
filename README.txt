This module allows the creation of an internal Drupal path that will direct the user to a unique path
based on the domain that the user is in. The link is based at the
path "/domain-redirect/12345" where "12345" is the redirect ID that is stored in the
database.

For example, let's say we have three domains on our site, and they all share the same primary links menu structure:

foo.example.com
bar.example.com
baz.example.com

Domain redirect will allow for one menu link to point to up to three different URLs, either internal or external. You can use one internal menu path "/domain-redirect/12345" and the user will be redirected to a different internal path or external URL depending on how the redirect is configured. So if the user sees the menu link at the "foo" subdomain, they might be redirected to http://www.lolcats.com/view/13285/, but at the "bar" subdomain, they would be redirected to "/node/353" and in any other subdomain, the link would redirect them to "node/2."

Thanks go out to other drupal coders, as I copied fervently, freely and unabashedly from the Path Redirect module.

Domain Redirect is developed and maintained by OpenSourcery.
